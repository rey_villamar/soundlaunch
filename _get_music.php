<?PHP
	require("dbase/config.inc.php");
	require("dbase/Database.class.php");
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	$db->connect();

	if(isset($_GET['start'])){
		$start = $_GET["start"];
	}else{
		$start = 100000;
	}
	$timeType = "alltime";
	
	//$sql = "Select * from ".MUSIC_TABLE."  where id < ".$start." and isactive = 1 order by id desc limit 5";
	$sql = "select * from tblMusicStats2 as A right outer join tblMusic as B on B.id = A.musicId where B.id < ".$start." and B.isactive = 1 order by A.".$timeType." DESC limit 5";

	$row = $db->query($sql);
	$json = array();	
	while ($record = $db->fetch_array($row)) 
	{
		$details = array
		(
			'music_id' => $record["id"],
			'music_title' => $record["title"],
			'music' => $record["music"],
			'poster' => $record["poster"],
			'comment' => $record["comment"],
			'isactive' => $record["isactive"]
		);
		array_push($json, $details);
	}
		
	//Encode the array into JSON.
	echo json_encode($json);
	mysql_close();

?>