
    
$(document).ready(function(){
	
	var arrOfMusic = [];
	var arrOfTitle = [];
	var arrOfCover = []
	var arrOfID = [];
	var isNext = "";
	var isNext = 0;
	var dislayItNone = "";
	var activePage = 1;
	var countMusic = 0;
	var arrOfBoxes = [];
	var isRandom = true;
	var pageList = 0;
	var arBoxes = [];
	var displayCounter = 24;
	var currentPage = 1;
	var previousSong = 1;
	var arActiveId = [];
	var pageNum = 1;
	var currentClickPage = $("#homeButton");
	var isTopRequestActive = false;
	var previousID = 0;
	var topRequestID = "";
	var arBoxShowed = [];
	$.ajax({ 
		type: 'POST', 
		url: '_fetchtoprequest.php', 
		data: { action: "getAllMusic",timeType:"week" }, 
		dataType: 'json',
		success: function (data) { 
		$.each(data, function(index, element) {
			//console.log("-------- "+element.title);
			arrOfID.push(element.id);
			arrOfMusic[element.id] = element.music;
			arrOfTitle[element.id] = element.title;
			arrOfCover[element.id] = "images/"+element.poster;
			countMusic++;
		});
		initMusic();
	}
	});



	//List of function
	//Make the player active
	makePlayActive = function (me)
	{
	$("#jp_container_1").show();
	console.log("Make Play Active ");
	    $('#left').attr("data-test", $('#center').attr('data-test'));
	    $('#left img').attr("src", $('#center img').attr('src'));
		previousID =  $('#center').attr('data-test');
		topRequestID =  $(me).attr('alt');
		
	    var myid = $(me).attr("data-id");
	    var imageSource = $(me).attr("data-img");
	    var songSource = $(me).attr("data-song");
	    

	    $('#center').attr('data-test', myid);
	   // $('#center').attr('data-img', imageSource);
	    $("#musicPlayer").jPlayer({
			  ended: function() { // The $.jPlayer.event.ended event
	
			     $("#right").click();
			  },
			size:{width: "565px",
	        height: "275px"},
			swfPath: "dist/jplayer",
			supplied: "mp3",
			wmode: "window",
			globalVolume: true,
			useStateClassSkin: true,
			autoBlur: true,
			smoothPlayBar: true,
			keyEnabled: true
		});
		
		 $("#musicPlayer").jPlayer("setMedia", {
					title: "Stirring of a fool",
					mp3: "music/"+songSource,
					poster: imageSource
		});
		$("#musicPlayer").jPlayer("play");
		//previousSong = myid;
		$.post( "_saveclick.php", { musicId: myid})
		.done(function( data ) {
			//console.log(" isTopRequestActive ----- "+isTopRequestActive)
				if(isTopRequestActive){

						console.log("TOP REQUEST INDEX"+topRequestID);
						$.ajax({
							type: 'POST', 
							url: '_fetchstats.php', 
							data: { action: "getOnlyRequest",id:myid }, 
							dataType: 'json',
							success: function (data) { 
								$.each(data, function(index, element) {
									$("#alltime"+topRequestID).html(element.alltime);
									$("#year"+topRequestID).html(element.year);
									$("#month"+topRequestID).html(element.month);
									$("#week"+topRequestID).html(element.week);
								});
							}
						});
				}
				
		});
		
		
					
	}
	

	generateRandom = function(){
		return arrOfID[Math.floor(Math.random() * arrOfID.length)];
	}
	function isInt(n) { 
			return parseInt(n) === n 
	};
	//End Functions
	
	//Start Music Fetching
	initMusic = function(){
		///For display music
		for(var i = 0; i < arrOfCover.length; i++){			
			if(arrOfTitle[arrOfID[i]] != null){	
		        $("#combobox").append('<option value="'+arrOfTitle[arrOfID[i]]+'">'+arrOfTitle[arrOfID[i]]+'</option>');
			}
		    arBoxShowed.push(i);
		    if(arrOfID[i] != null){	
				$("#itemContainer").append('<li> <div id="'+arrOfID[i]+'" alt="'+arrOfID[i]+'" data-id="'+arrOfID[i]+'" data-sequence="'+i+'" data-img="'+arrOfCover[arrOfID[i]]+'" data-song="'+arrOfMusic[arrOfID[i]]+'" onclick="makePlayActive(this)" class="'+i+' playlistMC'+dislayItNone+'  '+arrOfTitle[arrOfID[i]]+'" style = "width:250px;">'+
			'		                <div class="c-content-product-2 c-bg-white">'+
			'		                    <div class="c-content-overlay">'+
			'		                        <div id="cover'+i+'" class="c-bg-img-center c-overlay-object" data-height="height" style="height: 275px; background-repeat: no-repeat; background-position: left center; background-size: 100% 100%; background-image: url('+arrOfCover[arrOfID[i]].replace(/ /g,"%20")+');"></div>'+
			'		                    </div>'+
			'		                    <div class="c-info">'+
			'		                        <p id="title'+i+'"  class="c-title c-font-18 c-font-slim '+arrOfTitle[arrOfID[i]].replace(/ +/g, "")+'">'+arrOfTitle[arrOfID[i]]+'</p>'+
			'		                    </div>'+
			'		                </div>'+
			'		            </div></li>');
			}
			$("#"+i).click(function(){
				//alert("Send Data");
				//makePlayActive(this)
			});
			
            arActiveId.push(i);
            arBoxes.push(i);
			if(i%displayCounter == (displayCounter-1)){
				arrOfBoxes[pageList] = arBoxes;
				console.log("MODULOS: "+i%displayCounter+" PAGELIST "+ pageList+ "BOX LEN "+arrOfBoxes[pageList].length);
				arBoxes =[];
			}
	           // dislayItNone = "";
	    }
	    //End For display music
	    //Pagination
        $("div.holder").jPages({
          containerID  : "itemContainer",
          perPage      : 24,
          startPage    : 1,
          startRange   : 1,
          midRange     : 5,
          endRange     : 1
        });
   
	    totalPage = pageNum-1;
	    $("#pagingList").append('<li id="playNext" style="cursor:pointer;" class="c-next"><a><i class="fa fa-angle-right"></i></a></li>'+	
			'<li class="c-last"><a><i class="fa fa-angle-double-right"></i></a></li>');			
		function playNextMusic()
		{
		    console.log("Play Next Music");
		    var me = $("#"+isNext);
		    makePlayActive(me);
		}
					
        arrOfBoxes[pageList] = arBoxes;
        $("#musicPlayer").trigger('load');   

		
	    var tmpRand = 0;
	    //Left Image
		tmpRand = generateRandom();
		$("#left img").attr("src",arrOfCover[tmpRand]);
		$("#left").attr("data-test",tmpRand);
		//$("#left").attr("data",tmpRand);
		//$("#left").attr("data-img",arrOfCover[tmpRand]);
	    previousID = tmpRand;
		previousSong = tmpRand;
		
		//Center Image
		tmpRand = generateRandom();
		//$("#center img").attr("src",arrOfCover[tmpRand]);
		$("#center").attr("data-test",tmpRand);
		isNext = tmpRand;
		playNextMusic();
		
		//Right Image
		tmpRand = generateRandom();
		$("#right img").attr("src",arrOfCover[tmpRand]);
		$("#right").attr("data-test",tmpRand);
				 
		              
        $("#left").click(function()
        {
		     
			isNext = previousID;
	        playNextMusic();
	        
			
        });
        
        $("#right").click(function(){
			var id = $(this).attr("data-test");
			makePlayActive($("#"+id));
			var rand = generateRandom();
			$("#right img").attr("src",arrOfCover[rand]);
			$("#right").attr("data-test",rand);
			//$("#musicPlayer").jPlayer("play");
        });
        
        //TOP Section
        $('#center').click(function () {
			//$("#musicPlayer").jPlayer("play");
		});
		
		$('#center').live('mouseover', function () {
	    	//$("#musicPlayer").show();
	    	$(".jp-interface").show()
		});
		
		$('#center').live('mouseleave', function () {
		   // $("#musicPlayer").hide(); 
		   $(".jp-interface").hide()
		});
		
	    $("#playNext").click(function(){
			var tmpCurrentPage = currentPage + 1;
			visitPage(tmpCurrentPage);			
	    });
    
		//Paging Section
		$(".c-prev").click(function(){
			console.log("Clicked");
			var tmpCurrentPage = currentPage - 1;
			visitPage(tmpCurrentPage);
		});
		$(".c-first").click(function(){
			console.log("Visit First");
			visitPage(1);
		});
		$(".c-last").click(function(){
			console.log("Visit First");
			visitPage(totalPage);
		});
    

    changeColor = function(me)
    {
		$(me).parent().addClass("c-active");
		$(currentClickPage).parent().removeClass("c-active");
		currentClickPage = me;
    }
    
    $("#topRequestBtn").click(function(){
	    //$("#contentPage").hide();
	    $("#topPage").hide();
	    $("#lowerPage").hide();
	   $("#loadPageHere").load("toprequest.html");
	    changeColor(this);
	    $(document).prop('title', 'Top Requested Song');
	    isTopRequestActive = true;
    });

    
    
    $("#aboutBtn").click(function(){
	    $("#topPage").hide();
		$("#lowerPage").hide();
	    $("#loadPageHere").load("about_us.html");
	    changeColor(this);
	    $(document).prop('title', 'About Sound Launch');
    });
    
    
    $("#signBtn").click(function(){
	    $("#topPage").hide();
		$("#lowerPage").hide();
	    $("#loadPageHere").load("signin.html");
	    
	    $(document).prop('title', 'Sign In Sound Launch');
		Cookies.remove('email', { secure: true });
		Cookies.remove('id', { secure: true });
		$("#signBtn").html('Sign In<span class="c-arrow c-toggler"></span>')
		changeColor(this);
    });
    
    
    gotoUploadPage = function(){
	    
	     $("#topPage").hide();
		$("#lowerPage").hide();
	    $("#loadPageHere").load("uploadmusic.html");
	  //  changeColor(this);
	    $(document).prop('title', 'Upload your music');
	    $("#signBtn").html('Sign Out<span class="c-arrow c-toggler"></span>')
changeColor($("#launchBtn"));
	    
    }
    
      if(Cookies.get('email') != undefined){
		 $("#signBtn").html('Sign Out<span class="c-arrow c-toggler"></span>')
	}
	
    
	$("#homeButton").click(function(){
		$("#loadPageHere").empty();
		$("#topPage").show();
		$("#lowerPage").show();
		changeColor(this);
		$(document).prop('title', 'Welcome to Sound Launch');
		isTopRequestActive = false;
	});
	
	$("#bulletinBtn").click(function(){
		$("#topPage").hide();
		$("#lowerPage").hide();
	    $("#loadPageHere").load("bulletin.html");
	     changeColor(this);
	     $(document).prop('title', 'Bulletin Board');
	     load_music();
	     load_side_music();
    });
    
    $("#launchBtn").click(function(){
	     if(Cookies.get('email') != undefined){
		 $("#signBtn").html('Sign Out<span class="c-arrow c-toggler"></span>')
	
		$("#topPage").hide();
		$("#lowerPage").hide();
	    $("#loadPageHere").load("uploadmusic.html");
	     $(document).prop('title', 'Launch Music');
	    changeColor(this);
	    $("#signBtn").html('Sign Out<span class="c-arrow c-toggler"></span>')
	    }else{
		      $("#topPage").hide();
				$("#lowerPage").hide();
			    $("#loadPageHere").load("signin.html");
			    
			    $(document).prop('title', 'Sign In Sound Launch');
				Cookies.remove('email', { secure: true });
				Cookies.remove('id', { secure: true });
				 $("#signBtn").html('Sign In<span class="c-arrow c-toggler"></span>')
				 changeColor($("#signBtn"));
	    }
	    
	    
    });

			var vid = document.getElementById("musicPlayer");
			vid.onerror = function() {
				playNextMusic();
			};

/*
	$("#musicPlayer").onError(function(e){
		   switch (e.target.error.code) {
	     case e.target.error.MEDIA_ERR_ABORTED:
	       alert('You aborted the video playback.');
	       break;
	     case e.target.error.MEDIA_ERR_NETWORK:
	       alert('A network error caused the audio download to fail.');
	       break;
	     case e.target.error.MEDIA_ERR_DECODE:
	       alert('The audio playback was aborted due to a corruption problem or because the video used features your browser did not support.');
	       break;
	     case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
	       alert('The video audio not be loaded, either because the server or network failed or because the format is not supported.');
	       break;
	     default:
	       alert('An unknown error occurred.');
	       break;
	   }
	});
	
*/

 
    
    
	    visitPage = function(pageNum)
	    {
		    if(pageNum > 1)
		    {
			    $(".c-first").show();
			    $(".c-prev").show();
			}else{
				 $(".c-first").hide();
			    $(".c-prev").hide();
			}	    
			var startCount = (displayCounter * pageNum)-1;
			for( var j = 0; j < arActiveId.length; j++){
				//$("#"+arActiveId[j]).hide();
			}
			
		console.log("Number to HIDE "+arBoxShowed.length)
			for( var k = 0 ; k < arBoxShowed.length; k++){
				$("#"+arBoxShowed[k]).hide();
			}
			arActiveId = [];
			var i = 0;
			arBoxShowed = [];
			
			var endShow = displayCounter*pageNum;
			var startShow = displayCounter*(pageNum-1)
			console.log("Start "+startShow);
			console.log("endShow "+endShow);
			while(startShow < endShow){
				if(i < displayCounter){
					
				$("."+startShow).show();
				arBoxShowed.push(startCount);
				}
				arActiveId.push(startCount);
				startShow++;
				startCount = startCount - 1;
				i++;
			}
			 
/*
			var startHideCount = (displayCounter * currentPage)-1;
			i = 0;
			while(i < displayCounter){
				//$("#box"+startHideCount).hide();
				startHideCount = startHideCount - 1;
				i++;
			}
*/
			    
		    $("#btnPage"+pageNum).addClass("c-active")
		    $("#btnPage"+currentPage).removeClass("c-active")
		    if(pageNum == totalPage){
			    $(".c-next").hide();
			    $(".c-last").hide();
		    }else{
			    $(".c-next").show();
			    $(".c-last").show();
		    }
		    currentPage = pageNum;
	    }
		$(".jPag-control-front").css("left","96px");		
	}
	
	
	
	$("#searchInput").keyup(function(){
		console.log($(this).val());
	})   
	//End Of Music      

	function load_music(){
		$.ajax({ 
        type: 'POST', 
        url: '_get_music.php', 
        dataType: 'json',
        success: function (data) { 
            $.each(data, function(index, element) {
                //console.log("-------- "+element.title);
                // alert(element.music);
                $(".hfeed").append('<div class="post-outer">'+
                    '<div class="post hentry">'+
                    	'<div class="post-body entry-content">'+
                           '<div>'+
                              '<div class="article_container">'+  
                                 '<div class="article_image">'+
                                    '<img src="images/'+element.poster+'" width="230" height="230" data-song="'+element.music+'"  data-img="images/'+element.poster+'"  data-id="'+element.music_id+'" alt="'+element.music_id+'" onclick="makePlayActive(this)"  class="img-responsives">'+          
                                '</div>'+
                                 '<div class="article_inner">'+
                                    '<div class="article_header">'+
                                       '<h2>'+element.music_title+'</h2>'+
                                    '</div>'+
                                    '<div class="article_excerpt clearfix">'+element.comment+'</div>'+
                                    '<div class="" style = "font-size:12px;"><span class="article_comments">Comments (0)</span><span class="article_date">5/03/2015</span>'+
                                    '</div>'+
                                 '</div>'+
                              '</div>'+
                           '</div>'+
                           '<div class="article_footer clearfix">'+
                              '<div class="meta_date right">'+
                                 '<span class="article_timestamp">'+
                                 '<i class="fa fa-bookmark"></i>'+
                                 '5/03/2015'+
                                 '</span>'+
                              '</div>'+
                           '</div>'+
                           '<div style="clear: both;"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>');
            });
        }   
    });
    }

    function load_side_music(){
    	$.ajax({ 
        type: 'POST', 
        url: '_get_side_music.php', 
        dataType: 'json',
        success: function (data) { 
            $.each(data, function(index, element) {
            	$(".popular-posts").append('<ul>'+
                     '<li>'+
                        '<div class="item-content">'+
                            '<div class="item-thumbnail">'+
                                '<img src="images/'+element.poster+'" width="280" height="280" data-song="'+element.music+'"  data-id="'+element.music_id+'" alt="'+element.music_id+'" onclick="makePlayActive(this)"  class="img-responsives">'+          
                            '</div>'+
                            '<div class="item-title">'+element.music_title+'</div>'+
                            '<div class="item-snippet">'+
                                '<div class="item-content">'+
                                    '<div class="item-thumbnail">'+
                                    '</div>'+
                                    '<div class="item-title"></div>'+
                                '</div>'+
                                '<div style="clear: both;"></div>'+
                            '</div>'+
                        '</div>'+
                    '</li></ul>');
            });
        }   
    	});
    }

    $(".more").click(function(){
    	alert(1);
    	load_music();
    });
});

