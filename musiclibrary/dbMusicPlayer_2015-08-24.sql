# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.38)
# Database: dbMusicPlayer
# Generation Time: 2015-08-24 00:27:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tblMusic
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMusic`;

CREATE TABLE `tblMusic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblMusic` WRITE;
/*!40000 ALTER TABLE `tblMusic` DISABLE KEYS */;

INSERT INTO `tblMusic` (`id`, `filename`, `title`, `description`, `cover`)
VALUES
	(1,'Bagsakan by parokya ni edgar feat. francis m and gloc-9.mp3','one ','','images/Cover0.jpg'),
	(2,'Beyonce - Love On Top.mp3','two ','','images/Cover1.jpg'),
	(3,'Beyonce- Listen.mp3','three ','','images/Cover2.jpg'),
	(4,'Binibini - Noel Cabangon.mp3','four ','','images/Cover3.jpg'),
	(5,'DATI - OFFICIAL MUSIC VIDEO.mp3','five ','','images/Cover4.jpg'),
	(6,'David Guetta - Titanium ft. Sia.mp3','six ','','images/Cover5.jpg'),
	(7,'David Guetta - Without You ft. Usher.mp3','seven ','','images/Cover6.jpg'),
	(8,'Disney\'s Frozen Let It Go Sequence Performed by Idina Menzel.mp3','eight ','','images/Cover7.jpg'),
	(9,'DRAW ME CLOSE - WORSHIP JAMZ.mp3','nine ','','images/Cover8.jpg'),
	(10,'Hipon - Sir Rex Kantatero ft. Shehyee (Payphone Parody).mp3','ten','','images/Cover9.jpg'),
	(11,'I\'LL FLY AWAY (Lyrics) by Alison Krauss.mp3','eleven ','','images/Cover10.jpg'),
	(12,'Jireh Lim - Magkabilang Mundo (Lyrics).mp3','twelve ','','images/Cover11.jpg'),
	(13,'Just The Way You Are [OFFICIAL VIDEO] - Bruno Mars.mp3','thirteen ','','images/Cover12.jpg'),
	(14,'Katy Perry - Firework.mp3','fourteen ','','images/Cover13.jpg'),
	(15,'Let It Go.mp3','fifteen ','','images/Cover14.jpg'),
	(16,'Loonie feat. Quest - Tao Lang (Official Music Video).mp3','sixteen ','','images/Cover15.jpg'),
	(17,'Maja Salvador - Dahan-Dahan (Official Music Video).mp3','seventeen ','','images/Cover16.jpg'),
	(18,'Mariah Carey - Always Be My Baby.mp3','eighteen ','','images/Cover17.jpg'),
	(19,'Mariah Carey - Hero.mp3','nineteen ','','images/Cover18.jpg'),
	(20,'Mariah Carey - We Belong Together.mp3','twenty','','images/Cover19.jpg'),
	(21,'Mariah Carey - Whenever You Call.mp3','twenty one ','','images/Cover20.jpg'),
	(22,'Mariah Carey - Without You.mp3','twenty two ','','images/Cover21.jpg'),
	(23,'Mariah Carey-Reflection\'s (Care Enough) with lyrics.mp3','twenty three ','','images/Cover22.jpg'),
	(24,'Muzik One Studio Live Sessions - Fuerte Acoustic(OPM MEDLEY).mp3','twenty four ','','images/Cover23.jpg'),
	(25,'Nasa Iyo Na Ang Lahat by Daniel Padilla (Official Music Video).mp3','twenty five ','','images/Cover24.jpg'),
	(26,'Mariah Carey-Reflection\'s (Care Enough) with lyrics.mp3','twenty six',NULL,'images/Cover25.jpg');

/*!40000 ALTER TABLE `tblMusic` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
