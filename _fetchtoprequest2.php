<?PHP
require("dbase/config.inc.php");
require("dbase/Database.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
$timeType = $_POST["timeType"];


//Arhive Function

/*
$sqlLastYear = "INSERT INTO tblMusicStatsArchive select * from tblMusicStats where year(dataAdded)=year(CURRENT_DATE())-1";
$row = $db->query($sqlLastYear);
$sqlLastYear = "DELETE from tblMusicStats where year(dataAdded)=year(CURRENT_DATE())-1;";
$row = $db->query($sqlLastYear);
$insertToArchive = "INSERT INTO tblMusicStatsArchive select * from tblMusicStats where year(dataAdded)=year(CURRENT_DATE())-1";
*/




/*
$sql = "select A.id, A.title, A.music, A.poster, ifnull(B.week,0) as week, ifnull(C.month,0) as month, ifnull(D.year,0) as year, ifnull(E.alltime,0) as alltime from tblMusic as A left outer join
(Select musicId,count(week(CURRENT_DATE())=week(dataAdded)) as week from tblMusicStats where week(CURRENT_DATE())=week(dataAdded) group by week(dataAdded), musicId) as B
on A.id=B.musicId 
left outer join
(Select musicId,count(month(CURRENT_DATE())=month(dataAdded)) as month from tblMusicStats where month(CURRENT_DATE())=month(dataAdded) group by month(dataAdded), musicId) as C
on A.id=C.musicId 
left outer join
(Select musicId,count(year(CURRENT_DATE())=year(dataAdded)) as year from tblMusicStats where year(CURRENT_DATE())=year(dataAdded) group by year(dataAdded), musicId) as D
on A.id=D.musicId 
left outer join
(Select musicId,count(musicId) as alltime from tblMusicStats group by musicId) as E
on A.id=E.musicId ORDER BY ".$timeType." DESC";
*/

// $sql = "select * from (select A.id, A.title,
//  A.music, A.poster, ifnull(B.week,0) as week, ifnull(C.month,0) as month, ifnull(D.year,0) as year, ifnull
// (E.alltime,0) as alltime, A.isactive as active from tblMusic as A left outer join
// (Select musicId,count(week(CURRENT_DATE())=week(dataAdded)) as week from tblMusicStats where week(CURRENT_DATE
// ())=week(dataAdded) group by week(dataAdded), musicId) as B
// on A.id=B.musicId 
// left outer join
// (Select musicId,count(month(CURRENT_DATE())=month(dataAdded)) as month from tblMusicStats where month
// (CURRENT_DATE())=month(dataAdded) group by month(dataAdded), musicId) as C
// on A.id=C.musicId 
// left outer join
// (Select musicId,count(year(CURRENT_DATE())=year(dataAdded)) as year from tblMusicStats where year(CURRENT_DATE
// ())=year(dataAdded) group by year(dataAdded), musicId) as D
// on A.id=D.musicId 
// left outer join
// (Select '',A.musicId,if(B.alltime=NULL,0,B.alltime)+count(A.musicId) as alltime from tblMusicStats as A left outer join tblMusicArchive as B  on A.musicID=B.musicID group by A.musicID) as E
// on A.id=E.musicId) as A ORDER BY A.".$timeType." DESC";
//echo $sql;
//$sql = "SELECT * FROM tblMusicStats as A  INNER JOIN tblMusic as B  on A.musicID = B.id order by A.id DESC";

if(isset($_POST["limit"])){
	$limit = 5;
}else{
	$limit = 10000;
}
 $sql = "select * from tblMusicStats2 as A right outer join tblMusic as B on B.id = A.musicId where B.isactive = 1 order by A.".$timeType." DESC limit ".$limit." ";

$row = $db->query($sql);

$json = array();	
while ($record = $db->fetch_array($row)) 
{

	// if($record["active"] == "1"){
		
	$details = array
	(
		'id' => $record["id"],
		'title' => $record["title"],
		'music' => $record["music"],
		'poster' => $record["poster"],
		'artist'=> $record["artist"],
		'week' => $record["week"],
		'month' => $record["month"],
		'year' => $record["year"],
		'alltime' => $record["alltime"],

		//data for bulletin board sorting 
		'music_id' => $record["id"],
		'music_title' => $record["title"],
		'comment' => $record["comment"],
		'isactive' => $record["isactive"]
	);
	array_push($json, $details);
	// }
}
	
//Encode the array into JSON.
echo json_encode($json);

mysql_close();

?>