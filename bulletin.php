<?php
    require_once('components/header.php');  
    require("dbase/config.inc.php");
    require("dbase/Database.class.php");
?>


    <!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM --><style type="text/css">
    .ct-wrapper.clearfix .outer-wrapper .main-wrapper #content #Blog1 .blog-posts.hfeed .post-outer .post.hentry #post-body-1262565689809336873 #p1262565689809336873 .article_container .article_inner .article_header h2 a {
	text-decoration: none;
}
    .underline {
	text-decoration: none;
}
    </style>
    <center style="background-color: #999933; color: black;">



<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|Playfair+Display:300,400,700|Open+Sans:400,700|Montserrat:400,700|Pacifico&amp;subset=cyrillic">



<style type="text/css" id="page-skin-1">&lt;!--
html,
body,
div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
font,
img,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
figure {
    margin: 0;
    padding: 0;
}
article,
aside,
details,
figcaption,
figure,
header,
hgroup,
menu,
section {
    display: block;
}
table {
    border-collapse: separate;
    border-spacing: 0;
}
caption,
th,
td {
    text-align: left;
    font-weight: normal;
}
sup {
    vertical-align: super;
    font-size: smaller;
}
code {
    font-family: 'Courier New', Courier, monospace;
    font-size: 12px;
    color: #272727;
}
::selection {
    background: #333;
    color: #fff;
}
::-moz-selection {
    background: #333;
    color: #fff;
}
a img {
    border: none;
}
img {
    max-width: 100%;
    vertical-align: middle;
}
ol,
ul {
    padding: 10px 0 20px;
    margin: 0 0 0 35px;
    text-align: center;
}
ol li {
    list-style-type: decimal;
    padding: 0 0 5px;
}
ul li {
    list-style-type: square;
    padding: 0 0 5px;
}
ul ul,
ol ol {
    padding: 0;
}
h1,
h2,
h3,
h4,
h5,
h6 {
    font-family: Montserrat;
    font-weight: normal;
}
.post-body h1 {
    line-height: 40px;
    font-size: 42px;
    margin: 10px 0;
}
.post-body h2 {
    font-size: 36px;
    line-height: 44px;
    padding-bottom: 5px;
    margin: 10px 0;
}
.post-body h3 {
    font-size: 30px;
    line-height: 40px;
    padding-bottom: 5px;
    margin: 10px 0;
}
.post-body h4 {
    font-size: 26px;
    line-height: 36px;
    margin: 10px 0;
}
.post-body h5 {
    font-size: 20px;
    line-height: 30px;
    margin: 10px 0;
}
.post-body h6 {
    font-size: 16px;
    line-height: 24px;
    margin: 10px 0;
}
/*****************************************
Global Links CSS
******************************************/

.clr {
    clear: both;
    float: none;
}
.clearfix:before,
.clearfix:after {
    display: table;
    content: "";
    line-height: 0;
}
/*****************************************
Wrappers Stylesheet
******************************************/

.ct-wrapper {
    padding: 0px 15px;
    position: relative;
    max-width: 1200px;
    margin: 0 auto;
}
.outer-wrapper {
    margin: 45px 0 45px;
    position: relative;
}
.main-wrapper {
    float: left;
    width: 70.5%;
}
.sidebar-wrapper {
    width: 28%;
    float: right;
    margin-left: 15px;
}
div#content {} #content,
#sidebar {
    position: relative;
    width: 100%;
    display: inline-block;
}
/**** Layout Styling CSS *****/

body#layout .header-wrapper {
    margin-top: 40px;
}
body#layout .outer-wrapper,
body#layout .sidebar-wrapper,
body#layout .ct-wrapper,
#layout .main-wrapper {
    margin: 0;
    padding: 0;
}
body#layout #About {
    width: 100%;
}
#layout div#search-bar,
#layout .blog_social_block,
#layout .margin,
#layout .search_from,
body#layout {
    width: 900px
}
#layout div#header {
    width: 100%;
}
/*****************************************
Header CSS
******************************************/

#header {
    width: auto;
    padding: 35px 0;
    position: relative;
    float: left;
}
.header_row {
    position: relative;
}
div.header_row:after {
    content: "";
    position: absolute;
    width: 100%;
    height: 1px;
    background-color: #333333;
    bottom: 0;
}
#header-inner {
    margin: 0;
    padding: 0;
}
#header h1 {
    font-size: 32px;
    font-family: playfair display, sans-serif;
    font-style: normal;
    line-height: 1;
    display: block;
    letter-spacing: 0px;
    font-weight: 700;
    text-transform: capitalize;
}
.header h1 a,
.header h1 a:hover {
    color: #fff;
}
#header a {
    display: block;
    padding: 0;
    line-height: 1.3;
}
.header p.description {
    color: #333;
    font-size: 13px;
    font-style: italic;
    margin: 0;
    padding: 0;
    text-transform: capitalize;
}
#Header1 .description {
    display: none!important
}
.header img {
    border: 0 none;
    background: none;
    width: auto;
    height: auto;
    margin: 5px 0 0;
    max-height: 100px;
}
div#header-inner_image img {
    margin: 0 auto;
    max-height: 100px;
}
/*------secondary_Header -------*/

div#secondary_header {
    margin: 30px auto 0;
    text-align: center;
}
div#Heaedr2 #header-inner {
    display: inline-block;
}
div#Header2 h1 {
    font-family: pacifico, cursive;
    font-weight: normal;
    font-size: 80px;
    display: inline-block;
}
#Header2 h1 a {
    display: block;
    line-height: 1.8;
    padding: 0px 20px 0 5px;
}
#Header2 .descriptionwrapper {
    font-family: monospace;
    text-align: center;
    font-weight: 300;
    font-size: 12px;
}
div#header-inner_image {
    margin: 60px auto 25px;
    display: block;
}
/*****************************************
Header and General stylehseet
******************************************/

.margin {
    width: 100%;
    clear: both;
}
.margin-10 {
    margin-bottom: 10px;
}
.margin-20 {
    margin-bottom: 20px;
}
.margin-30 {
    margin-bottom: 30px;
}
.margin-50 {
    margin-bottom: 50px;
}
.margin-40 {
    margin-bottom: 40px;
}
.margin-60 {
    margin-bottom: 60px;
}
.margin-70 {
    margin-bottom: 70px;
}
.margin-80 {
    margin-bottom: 80px;
}
.margin-90 {
    margin-bottom: 90px;
}
.margin-100 {
    margin-bottom: 100px;
}
.margin-11 {
    margin-bottom: 110px;
}
.margin-120 {
    margin-bottom: 120px;
}
.top_header {
    background-color: #222;
    z-index: 99999;
}
header.blog_header {
    border: 0;
    padding: 0;
    padding-top: 40px;
    display: inline-block;
    float: left;
    width: 100%;
    background: #fff;
    margin: 0px;
    position: relative;
    z-index: 99;
    -moz-box-shadow: 0 0 1px 0 rgba(0, 0, 0, 0.33);
    -webkit-box-shadow: 0 0 1px 0 rgba(0, 0, 0, 0.33);
    box-shadow: 0 0 1px 0 rgba(0, 0, 0, 0.33);
    -webkit-transform: translateZ(0);
}
.header_wrapper {
    position: relative;
}
/*--------[ top-menu ]------------*/

.ct-transition {
    -webkit-transition: all 0.2s linear;
    -moz-transition: all 0.2s linear;
    -o-transition: all 0.2s linear;
    -transition: all 0.2s linear;
}
.blog_social_block a:hover i,
.ct-transition:hover {
    webkit-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -ms-transform: scale(1.1);
    -o-transform: scale(1.1);
    transform: scale(1.1);
}


/*****************************************
Main Section Stylesheet
******************************************/

.outer-wrapper,
.main-wrapper,
#content {
    box-shadow: none!important;
}
.main-wrapper {
    padding-top: 0;
}
.col,
main-col,
.side-col {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
}
.right {
    float: right;
}
.left {
    float: left;
}
.white {
    background: white;
}
.post-outer {
    margin-bottom: 30px;
}
.post-body {
    word-wrap: break-word;
}
.post {
    margin: 0;
    margin-bottom: 15px;
    background-color: white;
    position: relative;
}

/*------[ index page ]-------*/

.article_header h2 {
    font-size: 22px;
    line-height: 1.5;
    margin: 0;
    padding: 0;
    margin-bottom: 10px;
    letter-spacing: 0;
    text-transform: uppercase;
    font-weight: 700;
}
.article_header h2 a {
    color: #222;
}
.article_container {
    width: 100%;
    position: relative;
}
.playbutton,
.article_image {
    width: 45%;
    float: left;
    margin-bottom: 0;
    margin-right: 15px;
}
.article_image {
    position: relative;
}
.article_image img,
.article_slider img {
    width: 100%;
}
.article_inner {
    padding: 0;
    position: relative;
    width: 52%;
    float: left;
}
.article_excerpt {
    margin-bottom: 20px;
}
.article_footer {
    clear: both;
    padding: 0;
}
.article_footer .meta {
    position: relative;
    display: inline-block;
    font-style: italic;
}
.footer_meta {
    font-size: 12px;
    border-left: 5px solid #FF1870;
    padding-left: 10px;
}
.footer_meta &gt;
span {
    margin-right: 10px;
    position: relative;
}
.footer_meta &gt;
span:after {
    content: "|";
    padding-left: 10px;
}
.footer_meta &gt;
span:last-child:after {
    display: none;
}
.meta {
    width: 100%;
    display: block;
    margin-bottom: 10px;
    line-height: 1;
}
.meta i {
    margin-right: 3px;
}
.meta_right {
    width: auto;
    display: none;
}
.meta-item.categories a {
    color: #333
}
.meta-item.categories a:nth-child(n+2) {
    display: none;
}
span.article_timestamp i {
    margin-right: 5px;
}
span.article_timestamp {
    font-size: 12px;
    font-family: montserrat;
    color: #fff;
    opacity: 0.9;
    position: absolute;
    top: 20px;
    right: 20px;
    letter-spacing: 2px;
}
.post:hover span.article_timestamp {
    opacity: 1;
    transition: opacity .3s ease-in-out;
}
.meta-item.rdmre {
    display: none;
}
.meta-item.rdmre a {
    display: block;
    line-height: 1;
    font-family: montserrat;
    padding: 8px 15px;
    background-color: #FF435D;
    color: #fff;
    font-size: 12px;
    letter-spacing: 1px;
    text-transform: uppercase;
}
.meta-item.share a.button {
    color: #FEFEFE;
    padding: 2px 9px;
    display: block;
    font-size: 12px;
}
.meta-item.share {
    position: absolute;
    left: 0;
    bottom: 0;
    width: 30px;
    height: 30px;
    background-color: #FF1870;
}
ul.social-list li {
    margin-left: 5px;
    margin-right: 5px;
    display: inline-block;
    list-style: none;
}
.meta-item.share.active .social-list {
    bottom: 90px;
    opacity: 1;
    visibility: visible;
}
.meta-item.share .social-list {
    visibility: hidden;
    opacity: 0;
    margin: 0;
    width: 240px;
    position: absolute;
    padding: 0;
    left: 175px;
    bottom: 90px;
    text-align: center;
    border-radius: 5px;
    transform: translateX(-50%);
    -webkit-transform: translateX(-50%);
    -moz-transform: translateX(-50%);
    -o-transform: translateX(-50%);
    -ms-transform: translateX(-50%);
    z-index: 1000;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}
.social-list li a {
    width: 35px;
    height: 35px;
    text-align: center;
    line-height: 35px;
    border-radius: 28px;
    font-size: 14px;
    display: block;
    color: #fff;
    background-color: #FF1870!important;
}
.art_twitter {
    background: #43caf1 !important;
}
.art_facebook {
    background: #4e73d1 !important;
}
.art_google {
    background: #e63c3c !important;
}
.article_no_img .article_excerpt {
    margin-left: 0;
}
.article_excerpt p {
    font-weight: 400;
    font-size: 12px;
    letter-spacing: .5px;
    color: #555;
    font-family: playfair display;
}
.article_share_social:hover .article_share_social ul {
    display: inline-block;
}
.article_share_social {
    margin-top: 3px;
}
.article_share_social ul {
    padding: 0;
    margin: 0;
    text-align: right;
    display: none;
}
.article_share_social ul li {
    list-style: none;
    padding: 0;
    margin: 0;
    margin-left: 2px;
    display: inline-block;
}
.article_share_social ul li a {
    position: relative;
    display: block;
    text-align: center;
    -webkit-transition: all 0.1s linear;
    -moz-transition: all 0.1s linear;
    -o-transition: all 0.1s linear;
    -transition: all 0.1s linear;
}
.article_share_social ul li,
.article_share_social ul,
.share_toggle {
    float: left;
}
.article_share_social ul li i {
    width: 30px;
    height: 30px;
    font-size: 14px;
    color: #666;
    padding: 9px;
    display: block;
    background: #f0f0f0!important;
}
.share_toggle {
    display: inline-block;
    width: 30px;
    height: 30px;
    top: 0px;
    margin: 0px;
    margin-left: 2px;
    padding: 9px;
    background: #F0F0F0;
    color: #555555;
}
.article_share_social ul li a:after {
    content: attr(data-title);
    position: absolute;
    top: -33px;
    opacity: 0;
    visibility: hidden;
    font-size: 11px;
    line-height: 1;
    min-width: 70px;
    background: #ECECEC;
    color: #222;
    padding: 5px 10px;
    left: 0px;
}
.article_share_social ul li a:hover:after,
.article_share_social ul li a:hover:before {
    opacity: 1;
    visibility: visible;
    transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: .2s ease-in-out;
}
.article_share_social ul li a:before {
    content: "";
    position: absolute;
    border-style: solid;
    border-color: transparent;
    border-width: 7px;
    border-top-color: #ECECEC;
    top: -13px;
    opacity: 0;
    visibility: hidden;
}
.article_author a {
    color: #333;
}
/*--------[ item page ]-------*/

.article_post_title {
    font-size: 28px;
    font-weight: 700;
    line-height: normal;
    margin: 15px 0 20px;
    padding: 0;
    text-decoration: none;
    text-transform: uppercase;
}
.post-title a {
    color: #333;
}
.post-title a:hover {
    color: #2980B9;
}
.article_meta_container {
    font-size: 12px;
    text-transform: capitalize;
    margin: 0;
    margin-bottom: 25px;
    padding-bottom: 10px;
    font-family: montserrat;
    font-weight: 300;
}
.article_meta_container &gt;
span {
    margin-right: 15px;
}
.article_meta_container a,
.article_meta_container {
    color: #666;
}
.article_meta_container.post-header i {
    margin-right: 5px;
}
span.meta-item_categories a {
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #333;
    font-family: montserrat;
}
/*span.meta-item_categories a:nth-child(n+4){display:none;}*/

span.meta-item_categories {
    border-left: 5px solid #FF1870;
    font-size: 12px;
    font-weight: 700;
    padding-left: 10px;
}
.post-footer {
    margin-top: 30px;
}

/***** Page Nav CSS *****/

#blog-pager {
    display: inline-block;
    margin: 25px auto 0;
    overflow: visible;
    padding: 25px 0;
    width: 100%;
}
#blog-pager-newer-link {
    padding: 20px 0px;
    padding-right: 10px;
    position: relative;
    text-align: left;
    width: 50%;
}
#blog-pager-older-link {
    padding: 20px 0px;
    position: relative;
    text-align: right;
    width: 50%;
}
a#Blog1_blog-pager-older-link,
a#Blog1_blog-pager-newer-link {
    display: inline-block;
    background: #FF1870;
    line-height: 1;
    padding: 8px 13px;
    border-radius: 3px;
    font-size: 15px;
    color: #fff;
}
.showpageOf,
.home-link {
    display: none;
}
.showpagePoint {
    background: #FF1870;
    color: #FFFFFF;
    margin: 0 10px 0 0;
    padding: 5px 10px;
    text-decoration: none;
    border-radius: 3px;
    -moz-border-radius: 3px;
    -o-border-radius: 3px;
    -webkit-border-radius: 3px;
}
.showpage a,
.showpageNum a {
    background: #333;
    color: #FFFFFF;
    margin: 0 10px 0 0;
    padding: 5px 10px;
    text-decoration: none;
    border-radius: 3px;
    -moz-border-radius: 3px;
    -o-border-radius: 3px;
    -webkit-border-radius: 3px;
}
.showpage a:hover,
.showpageNum a:hover {
    background: #FF1870;
    color: #fff;
    border-radius: 3px;
    -moz-border-radius: 3px;
    -o-border-radius: 3px;
    -webkit-border-radius: 3px;
    text-decoration: none;
}
/*****************************************
Post Highlighter CSS
******************************************/

blockquote {
    border-color: #E6E6E6;
    border-style: solid;
    border-width: 1px;
    color: #6A6A6A;
    margin: 10px 0 20px;
    padding: 15px 20px 15px 55px;
    position: relative;
    font-size: 14px;
    background: #F3F3F3;
}
blockquote:after {
    content: "\f10d";
    font-family: fontawesome;
    font-style: normal;
    position: absolute;
    top: 5px;
    left: 15px;
    font-size: 24px;
    color: #FF1870;
}
/*****************************************
Sidebar stylesheet
******************************************/

#sidebar {
    margin: 0;
    padding: 0;
    display: block;
}
.footer h2 {
    font-size: 18px;
    margin-bottom: 30px;
    padding-bottom: 15px;
    text-transform: capitalize;
    color: #444;
    position: relative;
}
.sidebar h2 {
    font-size: 12px;
    font-weight: 700;
    line-height: 1;
    margin-bottom: 30px;
    text-transform: uppercase;
    color: #3D3D3D;
    position: relative;
    border-left: 5px solid #333;
    display: inline-block;
    padding-left: 10px;
    letter-spacing: 1px;
}
.sidebar .widget {
    padding: 5px 20px;
    clear: both;
    font-size: 13px;
    line-height: 23px;
    margin-bottom: 30px;
}
.sidebar ul {
    margin: 0;
    padding: 0;
    list-style: none;
}
.sidebar li {
    border-bottom: 1px solid #F1F1F1;
    line-height: normal;
    list-style: none !important;
    margin: 8px 0;
    overflow: hidden;
    padding: 0 0 10px;
}
.sidebar a {
    color: #333;
}

/*---- footer blog_menu ----*/

ul.blog_footer_menu {
    padding: 0;
    margin: 0;
}
ul.blog_footer_menu li {
    padding: 0;
    margin: 0;
    display: inline-block;
    margin-right: 0;
}
ul.blog_footer_menu li a {
    display: block;
    line-height: 1;
    padding: 0px 0px;
    padding-right: 15px;
    position: relative;
}
ul.blog_footer_menu li a:after {
    content: "|";
    position: absolute;
    right: 4px;
    top: 4px;
    font-size: 10px;
    color: #fff;
}
ul.blog_footer_menu li:last-child a:after {
    content: "";
}
/***** Custom Labels *****/

.cloud-label-widget-content {
    display: inline-block;
    text-align: left;
}
.cloud-label-widget-content .label-size {
    display: inline-block;
    float: left;
    font-size: 10px;
    font-family: Verdana, Arial, Tahoma, sans-serif;
    font-weight: bold;
    line-height: normal;
    margin: 5px 5px 0 0;
    opacity: 1;
    text-transform: uppercase;
}
.cloud-label-widget-content .label-size a {
    color: #000 !important;
    float: left;
    padding: 5px;
}
.cloud-label-widget-content .label-size:hover a {
    color: #555 !important;
}
.cloud-label-widget-content .label-size .label-count {
    color: #555;
    padding: 5px 0;
    float: left;
}
.sidebar .Label li {
    display: inline-block;
    margin: 0;
    padding: 0;
    border: 0;
}
.sidebar .Label li a {
    display: block;
    line-height: 1;
    padding: 2px 6px 4px;
    background: #FF1870;
    color: white;
    border-radius: 1px;
    font-size: 13px;
}
/*-----[ Popular Post ]------*/

.PopularPosts .item-thumbnail img {
    display: block;
    float: left;
    height: 90px;
    width: 90px;
    padding: 0;
    margin-right: 10px;
    margin-left: 10px;
    border: 5px solid transparent;
    box-shadow: 0px 0px 20px -5px #000;
    -moz-box-shadow: 0px 0px 20px -5px #000;
    -webkit-box-shadow: 0px 0px 20px -5px #000;
    -ms-box-shadow: 0px 0px 20px -5px #000;
    -o-box-shadow: 0px 0px 20px -5px #000;
}
.PopularPosts .item-title {
    padding-bottom: .2em;
    font-size: 13px;
    font-weight: normal;
}
.footer .popular-posts li:nth-child(n+4),
.item-snippet {
    display: none;
}
.footer .popular-posts img {
    margin-left: 0!important;
    width: 75px;
    height: auto;
    box-shadow: 0px 0px 20px -5px #000;
}
.sidebar .item-title a {
    color: #333;
}
/*----------[ Sidebar-social-share-icons ]--------------*/

.sidebar_social_icons ul {
    text-align: center;
}
.sidebar_social_icons li {
    display: inline-block;
    text-align: center;
    width: 18%;
    margin: 0;
    padding: 0;
    border: 0;
}
.sidebar_social_icons li a {
    display: inline-block;
    margin-bottom: 15px;
}
.sidebar_social_icons li a i {
    padding: 12px;
    font-size: 18px;
    color: #fff;
    width: 40px;
    height: 40px;
    position: relative;
    background: #FF1870 !important;
}
/***** Blogger Contact Form Widget *****/

.contact-form-email,
.contact-form-name,
.contact-form-email-message,
.contact-form-email:hover,
.contact-form-name:hover,
.contact-form-email-message:hover,
.contact-form-email:focus,
.contact-form-name:focus,
.contact-form-email-message:focus {
    background: #F8F8F8;
    border: 1px solid #D2DADD;
    box-shadow: 0 1px 1px #F3F4F6 inset;
    max-width: 300px;
    color: #999;
}
.contact-form-button-submit {
    background: #FF1870;
    border: medium none;
    outline: 0;
    float: right;
    height: auto;
    margin: 10px 0 0;
    max-width: 300px;
    padding: 5px 10px;
    width: 100%;
    cursor: pointer;
}
.contact-form-button-submit:hover {
    background: #FF1870!important;
}
/***** Profile Widget CSS *****/

.Profile img {
    border: 1px solid #cecece;
    background: #fff;
    float: left;
    margin: 5px 10px 5px 0;
    padding: 5px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}
.profile-data {
    color: #999999;
    font: bold 20px/1.6em Arial, Helvetica, Tahoma, sans-serif;
    font-variant: small-caps;
    margin: 0;
    text-transform: capitalize;
}
.profile-datablock {
    margin: 0.5em 0;
}
.profile-textblock {
    line-height: 1.6em;
    margin: 0.5em 0;
}
a.profile-link {
    clear: both;
    display: block;
    font: 80% monospace;
    padding: 10px 0;
    text-align: center;
    text-transform: capitalize;
}
/***** Meet The Author *****/

.vt_sidebar_author_item {
    margin-bottom: 0px;
    text-align: center;
}
.vt_sidebar_author_item .image-wrap {
    position: relative;
    overflow: hidden;
    border-radius: 50%;
    -webkit-transform: translate3d(0px, 0px, 0px);
    transform: translate3d(0px, 0px, 0px);
    width: 85%;
    height: auto;
    margin: 0 auto;
    margin-bottom: 15px;
}
.vt_sidebar_author_item .image-wrap:before {
    content: '';
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 10px solid #FF1870;
    border-image-source: initial;
    border-image-slice: initial;
    border-image-width: initial;
    border-image-outset: initial;
    border-image-repeat: initial;
    opacity: 0.95;
    margin: auto;
    top: 0px;
    right: 0px;
    bottom: 0px;
    left: 0px;
    -webkit-transition: all 0.4s linear;
    transition: all 0.4s linear;
}
.vt_sidebar_author_item .social {
    position: absolute;
    width: 92.5%;
    height: 92.5%;
    border-radius: 50%;
    background: #FF1870;
    opacity: 0;
    margin: auto;
    top: 0px;
    right: 0px;
    bottom: 0px;
    left: 0px;
    font-size: 0px;
    text-align: center;
    -webkit-transform: scale(0.5);
    transform: scale(0.5);
    -webkit-transition: all 0.3s linear;
    transition: all 0.3s linear;
}
.social.linear-3s .social-inner {
    position: absolute;
    width: 100%;
    padding: 15px 0;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}
.social-inner .fa {
    margin: 0px 5px;
    font-size: 18px;
    width: 30px;
    height: 30px;
    padding: 6px;
    border-radius: 50%;
    background: #f1f1f1!important;
    color: #333;
}
.image-wrap:hover .social.linear-3s {
    opacity: 0.95;
    -webkit-transform: scale(1);
    transform: scale(1);
}
.image-wrap:hover:before {
    -webkit-transform: scale(1.2);
    -moz-transform: scale(1.2);
    -ms-transform: scale(1.2);
    -o-transform: scale(1.2);
    transform: scale(1.2);
}
.vt_sidebar_author_item h4.author_name a {
    font-size: 18px;
    margin-bottom: 10px;
    display: block;
    color: #333;
}
.vt_sidebar_author_item p {
    line-height: 1.6;
    padding: 0 10px;
}

/*--------- [sidebar newsletter ]----------*/

div#blog_newsletter h5 {
    font-size: 14px;
    margin-bottom: 10px;
}
div#blog_newsletter p {
    font-size: 12px;
    line-height: 1.7;
    margin-bottom: 20px;
}
div#blog_newsletter input#subbox {
    line-height: 1;
    background: #2B2B2B;
    border: none;
    border-radius: 2px;
    font-size: 13px;
    letter-spacing: 1px;
    min-height: 30px;
    margin: 0 0 20px;
    padding: 10px 15px;
    width: 100%;
    box-shadow: none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    -ms-box-shadow: none;
    outline: 0;
    border: 1px solid #333;
}
div#blog_newsletter input#subbutton {
    padding: 10px;
    line-height: 1;
    width: 100%;
    text-transform: uppercase;
    margin-bottom: 5px;
    box-shadow: none;
    outline: 0;
    color: #fff;
    display: inline-block;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 13px;
    letter-spacing: 2px;
    font-weight: 400;
    background: #FF1870;
}
div#logo img {
    max-height: 30px;
    margin-bottom: 12px;
}
/*-----------[ share-wrapper ]-----------*/

.share-wrapper,
.authorboxwrap {
    margin-bottom: 50px;
}
.share-wrapper ul {
    padding: 0;
    margin: 0 auto;
    display: table;
    text-align: center;
}
.share-wrapper li {
    list-style: none;
    display: inline-block;
    float: left;
    margin-right: 0;
    padding: 0;
    margin-bottom: 30px;
}
.share-wrapper li:first-child {
    display: block;
    margin-bottom: 20px;
    font-size: 16px;
}
.share-wrapper li a {
    display: block;
    text-align: center;
}
.share-wrapper li a i {
    display: none;
    color: #555;
    width: 35px;
    height: 35px;
    padding: 9px;
    font-size: 16px;
    background: #F0F0F0!important;
    border: 1px solid #DEDEDE;
}
.share-wrapper {
    margin-bottom: 30px;
}
.share-wrapper span {
    display: block;
    font-size: 12px;
    font-family: montserrat;
    background-color: #eee;
    line-height: 1;
    padding: 8px 25px;
    color: #fff;
}
li.facebook_share span {
    background-color: #3b5998;
}
li.twitter_share span {
    background-color: #00aced;
}
li.pinterest_share span {
    background-color: #cb2027;
}
li.google_share span {
    background-color: #dd4b39;
}
li.linkedin_share span {
    background-color: #007bb6;
}
.share-wrapper &gt;
.title {
    text-align: center;
    margin-bottom: 30px;
}
/*------[author-box ]-------*/

.avatar-container {
    width: 170px;
    float: left;
}
.avatar-container img {
    width: 130px;
    height: auto;
}
.author_description_container {
    margin-left: 170px;
}
.author_description_container h4 {
    font-size: 16px;
    display: block;
    margin-bottom: 10px;
}
.author_description_container h4 a {
    color: #333;
}
.author_description_container p {
    font-size: 12px;
    line-height: 1.7;
    margin-bottom: 15px;
}
.authorsocial a {
    display: inline-block;
    margin-right: 5px;
    text-align: center;
    float: left;
    margin-right: 2px;
}
.authorsocial a i {
    width: 30px;
    height: 30px;
    padding: 8px 9px;
    display: block;
    background: #E9E9E9!important;
    color: #555;
}
/*------*|*|*| Related Posts *|*|*|----------*/

div#related-posts {
    font-size: 16px;
    display: inline-block;
    width: 100%;
}
div#related-posts h5 {
    font-size: 16px;
    text-transform: uppercase;
    margin: 0 0 25px;
    padding-bottom: 15px;
    font-weight: 900;
    letter-spacing: 1px;
    text-align: center;
    position: relative;
}
div#related-posts h5:after {
    content: "";
    position: absolute;
    width: 4px;
    height: 4px;
    background: #222;
    border-radius: 50%;
    bottom: 0;
    left: 47%;
    box-shadow: 1em 0px 0px 0px #222, 2em 0px 0px 0px #222;
}
div#related-posts ul {
    padding: 0;
    margin: 0;
}
div#related-posts ul li {
    list-style: none;
    display: block;
    float: left;
    width: 30.555%;
    max-width: 230px;
    padding: 0;
    margin-left: 25px;
    text-align: center;
    position: relative;
}
div#related-posts ul li:first-child {
    margin-left: 0;
}
div#related-posts img {
    padding: 0;
    border: 7px solid transparent;
    width: 230px;
    border: 5px solid transparent;
    box-shadow: 0px 0px 20px -5px #000;
    -moz-box-shadow: 0px 0px 20px -5px #000;
    -webkit-box-shadow: 0px 0px 20px -5px #000;
    -ms-box-shadow: 0px 0px 20px -5px #000;
    -o-box-shadow: 0px 0px 20px -5px #000;
}
a.related-thumbs {
    position: relative;
    display: block;
}
a.related-thumbs:before {
    opacity: 1;
}
a.related-title {
    font-weight: 400;
    font-size: 13px;
    line-height: 1.7;
    display: block;
    padding-top: 0;
    letter-spacing: 1px;
    margin: 10px 6px 0;
    color: #333;
}
/*****************************************
Responsive styles
******************************************/
/* responsive menu */

.vt_menu_toggle {
    position: absolute;
    top: 10px;
    left: 10px;
    font-size: 21px;
    display: none;
    z-index: 10000;
}
.vt_menu_toggle,
.vt_menu_toggle:hover,
.vt_menu_toggle:focus {
    color: #fff;
}
@media screen and (max-width: 960px) {
    .ct-wrapper {
        padding: 0 15px;
    }
    .main-wrapper {
        margin-right: 0;
        width: 100%;
    }
    .sidebar-wrapper {
        float: left;
        width: auto;
        margin-top: 30px;
    }
    .article_slider {
        max-width: 100%;
    }
    .article_slider img {
        max-width: 100%;
    }
    .vt_menu_toggle {
        display: inline-block;
    }
}
@media screen and (max-width: 840px) {
    ul.masona_ry {
        height: 500px;
    }
    #comment-editor {
        margin: 10px;
    }
    #header {
        text-align: center;
        padding-left: 5px;
    }
    div#search-button {
        height: 74px;
        padding-top: 35px;
    }
    .blog_social_block a {
        margin-right: 5px;
    }
    .blog_social_block a i {
        font-size: 14px;
        padding: 9px;
        width: 30px;
        height: 30px;
    }
    #header img {
        height: 60px;
        margin: 3px auto 0;
    }
    a.vt_menu_toggle {
        display: inline-block;
    }
    .blog_social_block {
        left: 40px;
    }
    ul.blog_menus {
        display: none;
        width: 230px;
        text-align: left;
        position: relative;
        box-shadow: 1px 1px 5px -2px #000;
        position: absolute;
        z-index: 9999;
        background: white;
        top: 55px;
    }
    ul.blog_menus li {
        border: 0;
        border-bottom: 1px solid #E4E4E4!important;
        display: block;
        width: 100%;
        list-style: none;
        padding: 0;
        margin: 0;
        position: relative;
    }
    .social_search {
        width: auto;
        margin-right: 0;
        padding-bottom: 7px;
    }
    .footer {
        width: 100%;
        margin: 0;
        margin-bottom: 30px;
    }
    div#related-posts ul li {
        width: 30%;
    }
}
@media screen and (max-width: 580px) {
    ul.masona_ry {
        height: 400px;
    }
    .article_inner {
        width: 100%;
    }
    div#search-bar {
        width: 100%;
        right: 20px;
        margin: 0 auto;
        max-width: 250px;
        padding-top: 35px;
        clear: both;
    }
    .article_image,
    .article_slider,
    .playbutton {
        width: 100%;
        float: none;
        margin-bottom: 20px!important;
    }
    .article_excerpt {
        margin-left: 0;
    }
    .footer {
        width: 100%;
        float: none;
        margin: 0;
        margin-bottom: 30px;
    }
    .meta-item.share .social-list {
        left: 250px;
        bottom: 120px;
    }
}
@media screen and (max-width: 420px) {
    .comments .comments-content .datetime {
        display: block;
        float: none;
    }
    .comments .comments-content .comment-header {
        height: 70px;
    }
}
@media screen and (max-width: 320px) {
    .ct-wrapper {
        padding: 0;
    }
    .post-body img {
        max-width: 230px;
    }
    .comments .comments-content .comment-replies {
        margin-left: 0;
    }
}
/*****************************************
Hiding Header Date and Feed Links
******************************************/

h2.date-header,
span.blog-admin {
    display: none!important;
}

.img-responsives{display: block;max-width: 100%;}
</style>


<!--<body>-->

<div class="clr"></div>
<div class="ct-wrapper clearfix">
   <div class="outer-wrapper">
      <div class="main-wrapper">
         <div class="featured_post">
            <div id="slider" class="slider section">
               <div id="HTML8" class="widget HTML">
                  <div id="main-slider" class="carouselle3 slider">
                  </div>
               </div>
            </div>
         </div>
         <div id="content" class="article section">
            <div id="Blog1" class="widget Blog">
               <div class="blog-posts hfeed">
                <?php 
                    $sql = "SELECT * FROM '".MUSIC_TABLE."' ";
                    $row = $db->query($sql);
                    var_dump($row);
                ?>
                  <div class="post-outer">
                     <div itemtype="http://schema.org/BlogPosting" itemscope="itemscope" itemprop="blogPost" class="post hentry">
                        <div id="post-body-1262565689809336873" class="post-body entry-content">
                           <div id="p1262565689809336873">
                              <div class="article_container">
                                 <div class="article_image">
                                    <a href="http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html" title=""><img src="images/CoverCrooked.jpg" width="280" height="280" class="img-responsives"></a>
                                   <!--  <div class="meta-item share">
                                       <ul class="social-list">
                                          <li><a target="_blank" rel="nofollow" oonclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://twitter.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html&amp;title=Ariana Grande’s Evolving Style" data-title="Twitter" class="art_twitter"><i class="fa fa-twitter ct-transition-up"></i></a></li>
                                          <li><a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://www.facebook.com/sharer.php?u=http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html&amp;title=Ariana Grande’s Evolving Style" data-title="Facebook" class="art_facebook"><i class="fa fa-facebook ct-transition-up"></i></a></li>
                                          <li><a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="https://plus.google.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html&amp;title=Ariana Grande’s Evolving Style" data-title="Google+" class="art_google"><i class="fa fa-google-plus ct-transition-up-up"></i></a></li>
                                       </ul>
                                       <a href="#social_share" class="button"><i class="fa fa-mail-forward"></i></a>
                                   </div> -->
                                </div>
                                 <div class="article_inner">
                                    <div class="article_header">
                                       <h2><a href="http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html" class="underline">Crooked - Anything Goes</a></h2>
                                    </div>
                                    <div class="article_excerpt clearfix">
                                       <p> A new direction in sound, with funk, soul, and a whole lot of jazz.  Check this group out in the near further.  Very good act.</p>
                                    </div>
                                    <div class="" style = "font-size:12px;"><span class="article_comments">Comments (0)</span><span class="article_date">5/04/2015</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="article_footer clearfix">
                              <div class="meta_date right">
                                 <span class="article_timestamp">
                                 <i class="fa fa-bookmark"></i>
                                 5/04/2015
                                 </span>
                              </div>
                           </div>
                           <script type="text/javascript">
                              var x= "Ariana Grande&amp;#8217;s Evolving Style";rm("p1262565689809336873","http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html","5/04/2015","0","&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/OOTD?max-results=6' rel='tag'&gt;OOTD&lt;/a&gt;","Lucky sao");
                           </script>
                           <div style="clear: both;"></div>
                        </div>
                     </div>
                  </div>
                 <!--  <div class="post-outer">
                     <div itemtype="http://schema.org/BlogPosting" itemscope="itemscope" itemprop="blogPost" class="post hentry">
                        <div id="post-body-5861606266670647449" class="post-body entry-content">
                           <div id="p5861606266670647449">
                              <div class="article_container">
                                 <div class="article_image">
                                    <a href="http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html" title=""><img src="images/CoverOperator.jpg" width="280" height="280" class="img-responsives"></a>
                                    <div class="meta-item share">
                                       <ul class="social-list">
                                          <li><a target="_blank" rel="nofollow" oonclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://twitter.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html&amp;title=Rancic hints she won’t return to ‘Fashion Police’" data-title="Twitter" class="art_twitter"><i class="fa fa-twitter ct-transition-up"></i></a></li>
                                          <li><a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://www.facebook.com/sharer.php?u=http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html&amp;title=Rancic hints she won’t return to ‘Fashion Police’" data-title="Facebook" class="art_facebook"><i class="fa fa-facebook ct-transition-up"></i></a></li>
                                          <li><a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="https://plus.google.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html&amp;title=Rancic hints she won’t return to ‘Fashion Police’" data-title="Google+" class="art_google"><i class="fa fa-google-plus ct-transition-up-up"></i></a></li>
                                       </ul>
                                       <a href="#social_share" class="button"><i class="fa fa-mail-forward"></i></a>
                                   </div>
                                </div>
                                 <div class="article_inner">
                                    <div class="article_header">
                                       <h2><a href="http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html" class="underline">Lin Salt - Welcome To Soundlaunch</a></h2>
                                    </div>
                                    <div class="article_excerpt clearfix">
                                       <p>
                                          A silky soul button, when pressed you can't help but call for the operate and beg for help. Oh yeah Lin. tell us what you need.</p></div>
                                    <div class="footer_meta meta"><span class="article_comments">Comments (0)</span><span class="article_date">5/03/2015</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="article_footer clearfix">
                              <div class="meta_date right">
                                 <span class="article_timestamp">
                                 <i class="fa fa-bookmark"></i>
                                 5/03/2015
                                 </span>
                              </div>
                           </div>
                           <script type="text/javascript">
                              var x= "Rancic hints she won&amp;#8217;t return to &amp;#8216;Fashion Police&amp;#8217;";rm("p5861606266670647449","http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html","5/03/2015","0","&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/fashion?max-results=6' rel='tag'&gt;fashion&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/Featured?max-results=6' rel='tag'&gt;Featured&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/Trend?max-results=6' rel='tag'&gt;Trend&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/TV?max-results=6' rel='tag'&gt;TV&lt;/a&gt;","Lucky sao");
                           </script>
                           <div style="clear: both;"></div>
                        </div>
                     </div>
                  </div>
                  <div class="post-outer">
                     <div itemtype="http://schema.org/BlogPosting" itemscope="itemscope" itemprop="blogPost" class="post hentry">
                        <div id="post-body-816962325449681339" class="post-body entry-content">
                           <div id="p816962325449681339">
                              <div class="article_container">
                                 <div class="article_image">
                                    <a href="http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html" title="">
                                    <img src="images/CoverNicoleO_Brandon.jpg" width="280" height="240" class="img-responsives"></a>
                                    <div class="meta-item share">
                                       <ul class="social-list">
                                          <li><a target="_blank" rel="nofollow" oonclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://twitter.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html&amp;title=Celebrity-inspired beauty tips for a fresh-faced spring" data-title="Twitter" class="art_twitter">
                                             <i class="fa fa-twitter ct-transition-up"></i></a>
                                          </li>
                                          <li><a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://www.facebook.com/sharer.php?u=http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html&amp;title=Celebrity-inspired beauty tips for a fresh-faced spring" data-title="Facebook" class="art_facebook"><i class="fa fa-facebook ct-transition-up"></i></a>
                                          </li>
                                          <li><a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="https://plus.google.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html&amp;title=Celebrity-inspired beauty tips for a fresh-faced spring" data-title="Google+" class="art_google"><i class="fa fa-google-plus ct-transition-up-up"></i></a>
                                          </li>
                                       </ul>
                                       <a href="#social_share" class="button"><i class="fa fa-mail-forward"></i></a>
                                    </div>
                                 </div>
                                 <div class="article_inner">
                                    <div class="article_header">
                                       <h2><a href="http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html" class="underline">Nicloe O'Brannon</a></h2>
                                    </div>
                                    <div class="article_excerpt clearfix">
                                       <p>
                                          Relaxing, smooth, and back around to relaxing...I like it!
                                       </p>
                                    </div>
                                    <div class="footer_meta meta"><span class="article_comments">Comments (0)</span><span class="article_date">5/03/2015</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="article_footer clearfix">
                              <div class="meta_date right">
                                 <span class="article_timestamp">
                                 <i class="fa fa-bookmark"></i>
                                 5/03/2015
                                 </span>
                              </div>
                           </div>
                           <script type="text/javascript">
                              var x= "Celebrity-inspired beauty tips for a fresh-faced spring";rm("p816962325449681339","http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html","5/03/2015","0","&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/fashion?max-results=6' rel='tag'&gt;fashion&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/Gossip?max-results=6' rel='tag'&gt;Gossip&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/News?max-results=6' rel='tag'&gt;News&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/Trend?max-results=6' rel='tag'&gt;Trend&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/TV?max-results=6' rel='tag'&gt;TV&lt;/a&gt;","Lucky sao");
                           </script>
                           <div style="clear: both;"></div>
                        </div>
                     </div>
                  </div>
                  <div class="post-outer">
                     <div itemtype="http://schema.org/BlogPosting" itemscope="itemscope" itemprop="blogPost" class="post hentry">
                        <div id="post-body-2411034240145872969" class="post-body entry-content">
                           <div id="p2411034240145872969">
                              <div class="article_container">
                                 <div class="article_image">
                                    <a href="http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html" title="">
                                    <img src="images/Cover38.jpg" width="280" height="240" class="img-responsives"></a>
                                    <div class="meta-item share">
                                       <ul class="social-list">
                                          <li>
                                             <a target="_blank" rel="nofollow" oonclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://twitter.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html&amp;title=Frozen’ star Idina Menzel has gone blonde like Elsa!" data-title="Twitter" class="art_twitter">
                                             <i class="fa fa-twitter ct-transition-up"></i>
                                             </a>
                                          </li>
                                          <li>
                                             <a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://www.facebook.com/sharer.php?u=http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html&amp;title=Frozen’ star Idina Menzel has gone blonde like Elsa!" data-title="Facebook" class="art_facebook"><i class="fa fa-facebook ct-transition-up"></i>
                                             </a>
                                          </li>
                                          <li><a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="https://plus.google.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html&amp;title=Frozen’ star Idina Menzel has gone blonde like Elsa!" data-title="Google+" class="art_google"><i class="fa fa-google-plus ct-transition-up-up"></i></a>
                                          </li>
                                       </ul>
                                       <a href="#social_share" class="button"><i class="fa fa-mail-forward"></i></a>
                                   </div>
                                </div>
                                 <div class="article_inner">
                                    <div class="article_header">
                                       <h2>
                                          <a href="http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html" class="underline">Bruik - Going Cross County</a>
                                       </h2>
                                    </div>
                                    <div class="article_excerpt clearfix">
                                       <p>"I wrote this while taking  a trip cross country in my old ford"</p>
                                       <p>Bruik</p>
                                   </div>
                                    <div class="footer_meta meta"><span class="article_comments">Comments (0)</span>
                                       <span class="article_date">5/03/2015</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="article_footer clearfix">
                              <div class="meta_date right">
                                 <span class="article_timestamp">
                                 <i class="fa fa-bookmark"></i>
                                 5/03/2015
                                 </span>
                              </div>
                           </div>
                           <script type="text/javascript">
                              var x= "Frozen&amp;#8217; star Idina Menzel has gone blonde like Elsa!";rm("p2411034240145872969","http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html","5/03/2015","0","&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/News?max-results=6' rel='tag'&gt;News&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/Trend?max-results=6' rel='tag'&gt;Trend&lt;/a&gt;","Lucky sao");
                           </script>
                           <div style="clear: both;"></div>
                        </div>
                     </div>
                  </div>
                  <div class="post-outer">
                     <div itemtype="http://schema.org/BlogPosting" itemscope="itemscope" itemprop="blogPost" class="post hentry">
                        <div id="post-body-4440685699487230604" class="post-body entry-content">
                           <div id="p4440685699487230604">
                              <div class="article_container">
                                 <div class="article_image">
                                    <a href="http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html" title="">
                                    <img src="images/CoverZulema.jpg" width="280" height="240" class="img-responsives"></a>
                                    <div class="meta-item share">
                                       <ul class="social-list">
                                          <li>
                                             <a target="_blank" rel="nofollow" oonclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://twitter.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html&amp;title=Jennie Garth is engaged to Dave Abrams!" data-title="Twitter" class="art_twitter">
                                             <i class="fa fa-twitter ct-transition-up"></i>
                                             </a>
                                          </li>
                                          <li>
                                             <a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="http://www.facebook.com/sharer.php?u=http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html&amp;title=Jennie Garth is engaged to Dave Abrams!" data-title="Facebook" class="art_facebook">
                                             <i class="fa fa-facebook ct-transition-up"></i>
                                             </a>
                                          </li>
                                          <li>
                                             <a target="_blank" rel="nofollow" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false" href="https://plus.google.com/share?url=http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html&amp;title=Jennie Garth is engaged to Dave Abrams!" data-title="Google+" class="art_google">
                                             <i class="fa fa-google-plus ct-transition-up-up"></i>
                                             </a>
                                          </li>
                                       </ul>
                                       <a href="#social_share" class="button"><i class="fa fa-mail-forward"></i></a>
                                    </div>
                                 </div>
                                 <div class="article_inner">
                                    <div class="article_header">
                                       <h2>
                                          <a href="http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html" class="underline">Zulema Cusseaux - Not For Me</a>
                                       </h2>
                                    </div>
                                    <div class="article_excerpt clearfix">
                                       <p>A tribute to a great artist, a West Tampa favorite, Zulema. I had pleasure of recording with this talent, vocal range; unreal. Rest in peace.</p>
                                    </div>
                                    <div class="footer_meta meta"><span class="article_comments">Comments (0)</span><span class="article_date">5/03/2015</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="article_footer clearfix">
                              <div class="meta_date right">
                                 <span class="article_timestamp">
                                 <i class="fa fa-bookmark"></i>
                                 5/03/2015
                                 </span>
                              </div>
                           </div>
                           <script type="text/javascript">
                              var x= "Jennie Garth is engaged to Dave Abrams!";rm("p4440685699487230604","http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html","5/03/2015","0","&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/fashion?max-results=6' rel='tag'&gt;fashion&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/Featured?max-results=6' rel='tag'&gt;Featured&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/News?max-results=6' rel='tag'&gt;News&lt;/a&gt;&lt;a href='http://inspiro-veethemes.blogspot.in/search/label/TV?max-results=6' rel='tag'&gt;TV&lt;/a&gt;","Lucky sao");
                           </script>
                           <div style="clear: both;"></div>
                        </div>
                     </div>
                  </div> -->
               </div>
               <div id="blog-pager" class="blog-pager">
                  <span id="blog-pager-older-link">
                  <a title="Older Posts" id="Blog1_blog-pager-older-link" href="http://inspiro-veethemes.blogspot.in/search?updated-max=2015-05-03T23:42:00-07:00&amp;max-results=5" class="blog-pager-older-link">More Artists</a>
                  </span>
                  <a href="http://inspiro-veethemes.blogspot.in/" class="home-link">Home</a>
               </div>
               <div class="clear"></div>
               <script type="text/javascript">window.___gcfg = {'lang': 'en'};</script>
            </div>
         </div>
      </div>
      <!-- /main-wrapper -->
      <div class="sidebar-wrapper">
         <div id="sidebar" class="sidebar col section">
            <div id="PopularPosts100" class="widget PopularPosts">
               <h3 style = "font-size:14px !important;">Popular Music</h3>
               <div class="widget-content popular-posts">
                  <ul>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html">
                              <img width="280" border="0" height="280" src="images/WhereAreYou.jpg" alt="">
                              </a>
                           </div>
                           <div class="item-title"><a href="http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style.html" class="underline">Platan Rich - Where Are You</a></div>
                           <div class="item-snippet">   Hi, I'm Platan Rich, and I hope you enjoy my music.
                             <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html"></a>
                           </div>
                           <div class="item-title"></div>
                            </div>
                        <div style="clear: both;"></div>
                     </div></div></li>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html">
                              <img width="280" border="0" height="280" src="images/CoverCherlyCotton.jpg" alt="">
                              </a>
                           </div>
                           <div class="item-title"><a href="http://inspiro-veethemes.blogspot.in/2015/05/jennie-garth-is-engaged-to-dave-abrams.html" class="underline">Cherly Cotton - Cotton</a></div>
                           <div class="item-snippet">I'm Cherly Cotton, I hope you are enjoying me on SOUNDLAUNCH.COM,</div></div>
                     </li>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html">
                              <img width="280" border="0" height="280" src="images/Car1.jpg" alt="">
                              </a>
                           </div>
                           <div class="item-title"><a href="http://inspiro-veethemes.blogspot.in/2015/05/frozen-star-idina-menzel-has-gone.html" class="underline">Mama Tute - Branson Street</a></div>
                           <div class="item-snippet">I like this song, simple and fun!</div>
                        </div>
                        <div style="clear: both;"></div>
                     </li>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html">
                              <img width="280" border="0" height="280" src="images/AmysSong.jpg" alt="">
                              </a>
                           </div>
                           <div class="item-title"><a href="http://inspiro-veethemes.blogspot.in/2015/05/celebrity-inspired-beauty-tips-for.html" class="underline">The 13th Floor Band - Amy's Song</a></div>
                           <div class="item-snippet">Petty cool. I sitting around listening at first then i got envolved with different kinds of beats (rap, soul, alternative, etc.), i really like the afro-cuba thing, funky.   Really man, I just listen and press keys.</div></div>
                     </li>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/julia-roberts-is-new-face-of-givenchy.html">
                              <img width="280" border="0" height="280" src="images/CoverGermantownBoys.jpg" alt="">
                              </a>
                           </div>
                           <div class="item-title"><a href="http://inspiro-veethemes.blogspot.in/2015/05/julia-roberts-is-new-face-of-givenchy.html" class="underline">The Germantown Boys - Rock Then Bend My Knees</a></div>
                           <div class="item-snippet">Young rapper telling it how they see it.</div>
                        </div>
                        <div style="clear: both;"></div>
                     </li>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/5-things-your-man-should-know-about.html">
                              <img width="280" border="0" height="280" src="images/CoverToshiSadao.jpg" alt="">
                              </a>
                           </div>
                           <div class="item-title"><span class="underline"><a href="http://inspiro-veethemes.blogspot.in/2015/05/5-things-your-man-should-know-about.html" class="underline">Toshi Sadao - Dress Rehearsal</a></span><a href="http://inspiro-veethemes.blogspot.in/2015/05/5-things-your-man-should-know-about.html"><br />
                          </a></div>
                           <div class="item-snippet">Alternative-Funk-Jazz-Acid-Soul kind of a thing.</div>
                        </div>
                        <div style="clear: both;"></div>
                     </li>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html"><img width="280" border="0" height="280" src="images/CoverEvansVille.jpg" alt="" /></a><a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/dakota-johnson-debuts-new-short-do.html"></a>
                           </div>
                           <div class="item-title">
                             <div class="item-title"><a href="http://inspiro-veethemes.blogspot.in/2015/05/rancic-hints-she-wont-return-to-fashion.html" class="underline">Jose Morales - Evansville</a></div>
                             <div class="item-snippet">A city beat that I like.</div>
                           </div>
                        </div>
                        <div style="clear: both;"></div>
                     </li>
                     <li>
                        <div class="item-content">
                           <div class="item-thumbnail">
                              <a target="_blank" href="http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style_3.html">
                              <img width="280" border="0" height="280" src="images/Blues.jpg" alt="">
                              </a>
                           </div>
                           <div class="item-title"><a href="http://inspiro-veethemes.blogspot.in/2015/05/ariana-grandes-evolving-style_3.html" class="underline">Evans Bule - Blues</a></div>
                           <div class="item-snippet">It's pronounced &quot;Evans Blues&quot;, we're different, but I hope you enjoy.</div>
                        </div>
                        <div style="clear: both;"></div>
                     </li>
                  </ul>
               </div>
            </div>
           <!--  <div id="Label2" class="widget Label">
               <h2>&nbsp;</h2>
               <div class="widget-content list-label-widget-content"> </div>
            </div>
            <div id="BlogArchive1" class="widget BlogArchive">
               <h2>Archive</h2>
               <div class="widget-content">
                  <div id="ArchiveList">
                     <div id="BlogArchive1_ArchiveList">
                        <ul class="flat">
                           <li class="archivedate">
                              <a href="http://inspiro-veethemes.blogspot.in/2015_05_01_archive.html">May</a> (9)
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="clear"></div>
                  <span class="widget-item-control">
                  <span class="item-control blog-admin">
                  <a title="Edit" target="configBlogArchive1" onclick="return _WidgetManager._PopupConfig(document.getElementById(&quot;BlogArchive1&quot;));" href="//www.blogger.com/rearrange?blogID=1825417143071665485&amp;widgetType=BlogArchive&amp;widgetId=BlogArchive1&amp;action=editWidget&amp;sectionId=sidebar" class="quickedit">
                  <img width="18" height="18" src="//img1.blogblog.com/img/icon18_wrench_allbkg.png" alt="">
                  </a>
                  </span>
                  </span>
                  <div class="clear"></div>
               </div>
            </div> -->
            <div id="HTML2" class="widget HTML">
               <div class="widget-content">
                  <h3 style = "font-size:12px !important;">Social Share Icons</h3>
                  <div class="sidebar_social_icons">
                     <ul class="clearfix">
                        <li class="facebook-social">
                           <a title="Facebook" target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook ct-transition"></i></a>
                           <span title="Fans" class="ct-counter">19,701</span>
                        </li>
                        <li class="twitter-social">
                           <a title="Twitter" target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter ct-transition"></i></a>
                           <span title="Followers" class="ct-counter">9,297</span>
                        </li>
                        <li class="Linkedin-social">
                           <a title="Linkedin" target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin ct-transition"></i></a>
                           <span title="Subscribers" class="ct-counter">4,182</span>
                        </li>
                        <li class="pinterest-social">
                           <a title="Pinterest" target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest ct-transition"></i></a>
                           <span title="Subscribers" class="ct-counter">2,157</span>
                        </li>
                        <li class="rss-social">
                           <a title="Rss" target="_blank" href="http://www.rss.com"><i class="fa fa-rss ct-transition"></i></a>
                           <span title="Subscribers" class="ct-counter">1,052</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /sidebar-wrapper -->
      <div class="clr"></div>
   </div>
   <!-- /outer-wrapper -->
</div>
<!-- /ct-wrapper -->

<!-- footer-credits -->
<script type="text/javascript">
  /*&lt;![CDATA[*/
    var postperpage=5;
    var numshowpage=2;
    var upPageWord ='&lt;i class="fa fa-angle-double-left"&gt;&lt;/i&gt;';
    var downPageWord ='&lt;i class="fa fa-angle-double-right"&gt;&lt;/i&gt;';
    var urlactivepage=location.href;
    var home_page="/";
  /*]]&gt;*/
</script>

<script type="text/javascript">//&lt;![CDATA[


$(document).ready(function(){
	
var smooth_scroll = $('#smooth_scroll');
	//Click event to scroll to top
	smooth_scroll.click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});

var menu_toggle = $('.vt_menu_toggle');
menu_toggle.click(function(){
	  $("ul.blog_menus").slideToggle();
  	});

$('body').on("click",".meta-item.share a.button,.showMoreOptions",function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).parents('.meta-item.share').addClass('active');
		$('.meta-item.share').parents(".article_image")
	});

$('body').click(function(e){
    if($(e.target).closest('.meta-item.share').length === 0){
      $('.meta-item.share').removeClass('active');
	$('.meta-item.share').parents(".article_image")
    }
    });

});
//]]&gt;</script>
</script>



    </center>   <!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
    <!-- END: CONTENT/USER/SIGNUP-FORM -->
    <!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
    <div class="modal fade c-content-login-form" id="login-form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content c-square">
                <div class="modal-header c-no-border">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                    <p>Let's make today a great day!</p>
                    <form>
                        <div class="form-group">
                            <label for="login-email" class="hide">Email</label>
                            <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="login-password" class="hide">Password</label>
                            <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <div class="c-checkbox">
                                <input type="checkbox" id="login-rememberme" class="c-check">
                                <label for="login-rememberme" class="c-font-thin c-font-17">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                            <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                        </div>
                        <div class="clearfix">
                            <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                                <span>or signup with</span>
                            </div>
                            <ul class="c-content-list-adjusted">
                                <li>
                                    <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                        <i class="fa fa-twitter"></i> Twitter
                                    </a>
                                </li>
                                <li>
                                    <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                        <i class="fa fa-facebook"></i> Facebook
                                    </a>
                                </li>
                                <li>
                                    <a class="btn btn-block c-btn-square btn-social btn-google">
                                        <i class="fa fa-google"></i> Google
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="modal-footer c-no-border">
                    <span class="c-text-account">Don't Have An Account Yet ?</span>
                    <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
                </div>
            </div>
        </div>
    </div>

                </center>
            </div>
        </div>
    </div>



   
    <script>
        $(document).ready(function () {

            var slider = $('.c-layout-revo-slider .tp-banner');
            var cont = $('.c-layout-revo-slider .tp-banner-container');
            var height = (App.getViewPort().width < App.getBreakpoint('md') ? 1024 : 620);

            var api = slider.show().revolution({
                delay: 15000,
                startwidth: 1170,
                startheight: height,

                navigationType: "hide",
                navigationArrows: "solo",

                touchenabled: "on",
                onHoverStop: "on",

                keyboardNavigation: "off",

                navigationStyle: "circle",
                navigationHAlign: "center",
                navigationVAlign: "center",

                fullScreenAlignForce: "off",

                shadow: 0,
                fullWidth: "on",
                fullScreen: "off",

                spinner: "spinner2",

                forceFullWidth: "on",
                hideTimerBar: "on",

                hideThumbsOnMobile: "on",
                hideNavDelayOnMobile: 1500,
                hideBulletsOnMobile: "on",
                hideArrowsOnMobile: "on",
                hideThumbsUnderResolution: 0,

                videoJsPath: "rs-plugin/videojs/",
            });
        }); //ready
    </script>

   