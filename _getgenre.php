<?PHP
require("dbase/config.inc.php");
require("dbase/Database.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

$sql = "Select * from tblgenre order by genreID desc";
$row = $db->query($sql);
$json = array();	
while ($record = $db->fetch_array($row)) 
{
	$details = array
	(
		'id' => $record["genreID"],
		'genre' => $record["genre"]
	);
	array_push($json, $details);
}
	
//Encode the array into JSON.
echo json_encode($json);
mysql_close();


?>