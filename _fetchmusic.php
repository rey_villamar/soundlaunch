<?PHP
require("dbase/config.inc.php");
require("dbase/Database.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

$sql = "Select * from ".MUSIC_TABLE."  as A  left join tblgenre as B on B.genreID = A.genreID order by dateadded desc";
$row = $db->query($sql);
$json = array();	
while ($record = $db->fetch_array($row)) 
{
	$genre = ($record["genre"] == null ?  ""  : $record["genre"]);
	$details = array
	(
		'id' => $record["id"],
		'title' => $record["title"],
		'music' => $record["music"],
		'poster' => $record["poster"],
		'comment' => $record["comment"],
		'isactive' => $record["isactive"],
		'tags' => $record["tags"],
		'artist' => $record["artist"],
		'genre' => $genre
	);
	array_push($json, $details);
}
	
	
//Encode the array into JSON.
echo json_encode($json);
mysql_close();

?>